﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fifteen;
using NUnit.Framework;

namespace FifteenTests
{
    [TestFixture]
    public class ResultListTests
    {
        private IResultList _resultList;
        private DateTime _dateNow;

        [SetUp]
        public void SetUp()
        {
            _resultList = new ResultList();
            _dateNow = DateTime.Now;
        }

        [Test]
        public void AddRecord_AddsRecord()
        {
            _resultList.AddRecord(10, 5, _dateNow);
            var record = _resultList.AllRecords.First();
            Assert.AreEqual(new Record {Moves = 10, Seconds = 5, Date = _dateNow}, record);
        }

        [Test]
        public void DeleteRecordsBefore_DeletesRecordsBeforeDate()
        {
            _resultList.AddRecord(10, 5, _dateNow - new TimeSpan(1, 0, 0));
            _resultList.AddRecord(20, 10, _dateNow + new TimeSpan(1, 0, 0));
            _resultList.DeleteRecordsBefore(_dateNow);
            Assert.AreEqual(1, _resultList.AllRecords.Count());
            var actual = _resultList.AllRecords.First();
            var expected = new Record {Moves = 20, Seconds = 10, Date = _dateNow + new TimeSpan(1, 0, 0)};
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BestMovesRecords_ReturnsRecordsSortedByMoves()
        {
            _resultList.AddRecord(10, 1, _dateNow);
            _resultList.AddRecord(9, 1, _dateNow);
            _resultList.AddRecord(15, 1, _dateNow);
            _resultList.AddRecord(1, 1, _dateNow);
            var expected = new List<Record>
            {
                new Record(1, 1, _dateNow),
                new Record(9, 1, _dateNow),
                new Record(10, 1, _dateNow),
                new Record(15, 1, _dateNow)
            };
            var actual = _resultList.TakeBestMovesRecords();
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void BestSecondsRecords_ReturnsRecordsSortedBySeconds()
        {
            _resultList.AddRecord(1, 10, _dateNow);
            _resultList.AddRecord(1, 20, _dateNow);
            _resultList.AddRecord(1, 15, _dateNow);
            _resultList.AddRecord(1, 30, _dateNow);
            var expected = new List<Record>
            {
                new Record(1, 10, _dateNow),
                new Record(1, 15, _dateNow),
                new Record(1, 20, _dateNow),
                new Record(1, 30, _dateNow)
            };
            var actual = _resultList.TakeBestSecondsRecords();
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}