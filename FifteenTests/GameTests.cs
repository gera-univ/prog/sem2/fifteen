﻿using Fifteen;
using NUnit.Framework;

namespace FifteenTests
{
    [TestFixture]
    public class GameTests
    {
        private Game _game;

        [SetUp]
        public void SetUp()
        {
            _game = new Game();
        } 

        [Test]
        public void Ctor_SetsInitialGameState()
        {
            AssertInitialState();
        }

        [Test]
        public void Reset_ResetsGameState()
        {
            _game.Reset();

            AssertInitialState();
        }

        [Test]
        public void Move_WhenCellHasNoEmptyNeighbor_DoesNothing()
        {
            _game.Move("5");

            AssertInitialState();
        }

        [Test]
        public void Move_WhenCellHasEmptyNeighbor_ExchangesCurrentAndEmptyCells()
        {
            AssertAfterMove("E", "0123456789ABCDFE");
            AssertAfterMove("B", "0123456789AFCDEB");
        }

        private void AssertAfterMove(string cell, string expected)
        {
            _game.Reset();
            _game.Move(cell);
            Assert.AreEqual(expected, _game.State);
        }

        [Test]
        public void GetPosition_ReturnsPosition()
        {
            Assert.AreEqual(15, _game.GetPosition("F"));
            Assert.AreEqual(1, _game.GetPosition("1"));
            Assert.AreEqual(10, _game.GetPosition("A"));
        }

        [Test]
        public void HasEmptyNeighbor_WhenEmptyCellIsNearby_ReturnsTrue()
        {
            Assert.IsTrue(_game.HasEmptyNeighbor("E"));
            Assert.IsTrue(_game.HasEmptyNeighbor("B"));
        }

        [Test]
        public void HasEmptyNeighbor_WhenEmptyCellIsFarAway_ReturnsFalse()
        {
            Assert.IsFalse(_game.HasEmptyNeighbor("A"));
            Assert.IsFalse(_game.HasEmptyNeighbor("0"));
        }

        private void AssertInitialState()
        {
            Assert.AreEqual("0123456789ABCDEF", _game.State);
        }
    }
}
