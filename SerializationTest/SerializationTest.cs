﻿using System;
using System.Collections.Generic;
using Fifteen;

namespace SerializationTest
{
    class SerializationTest
    {
        private static void PrintGameRecords(IGame game)
        {
            Console.WriteLine("All records:");
            PrintRecordCollection(game.Records);

            Console.WriteLine("Top 10 results by time");
            PrintRecordCollection(game.TakeBestSecondsRecords());

            Console.WriteLine("Top 10 results by moves");
            PrintRecordCollection(game.TakeBestMovesRecords());
        }

        private static void PrintRecordCollection(IEnumerable<Record> records)
        {
            foreach (Record record in records)
            {
                Console.WriteLine($"{record.Moves} {record.Seconds} {record.Date}");
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Testing serialization of game results.");

            //var game1 = new Game {ResultSerializer = new XMLResultSerializer(), FileName = "results.xml"};
            var game1 = new Game();
            game1.MakeRecord(0);
            game1.MakeRecord(15);
            game1.Move("E");
            game1.MakeRecord(5);
            game1.MakeRecord(10);
            game1.Move("A");
            game1.MakeRecord(1000);
            game1.MakeRecord(20);
            game1.MakeRecord(1);
            game1.MakeRecord(-40);
            game1.MakeRecord(250);
            game1.MakeRecord(111);
            game1.MakeRecord(215);

            Console.WriteLine("Results before serialization");
            PrintGameRecords(game1);

            game1.SaveResults();

            //var game2 = new Game {ResultSerializer = new XMLResultSerializer(), FileName = "results.xml"};
            var game2 = new Game();
            game2.LoadResults();

            Console.WriteLine("Results after serialization");
            PrintGameRecords(game2);
        }
    }
}