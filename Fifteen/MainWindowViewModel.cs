﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;

namespace Fifteen
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IGame _game;

        private bool _isGameStarted;

        private string _currentTime;

        private DispatcherTimer _timer;
        private static int _seconds;

        public List<CellViewModel> Cells { get; } = new List<CellViewModel>();

        public ICommand ShuffleCommand { get; }
        public ICommand ResetCommand { get; }
        public ICommand ShowResultsCommand { get; }

        private readonly MediaPlayer _mediaPlayer = new MediaPlayer();
        private bool _isResetEnabled;

        public bool IsGameStarted
        {
            get => _isGameStarted;
            set
            {
                _isGameStarted = value;
                RaisePropertyChanged(nameof(IsGameStarted));
            }
        }

        public bool IsResetEnabled
        {
            get => _isResetEnabled;
            set
            {
                _isResetEnabled = value;
                RaisePropertyChanged(nameof(IsResetEnabled));
            }
        }

        public string CurrentTime
        {
            get => _currentTime;
            set
            {
                if (_currentTime == value)
                    return;
                _currentTime = value;
                RaisePropertyChanged(nameof(CurrentTime));
            }
        }

        public bool Parity => _game.IsEven();

        public MainWindowViewModel(IGame game)
        {
            _game = game;

            MessengerInstance.Register<ClickedMessage>(this, OnClickedMessage);
            ShuffleCommand = new RelayCommand<string>(OnShuffleCommand);
            ResetCommand = new RelayCommand<string>(OnResetCommand);
            ShowResultsCommand = new RelayCommand<string>(OnShowResults);

            InitializeGame();
            InitializeTimer();
        }

        private void OnShowResults(string obj)
        {
            var resultsControl = new ResultsControl();
            var resultsViewModel = new ResultsControlViewModel(_game);
            resultsControl.DataContext = resultsViewModel;
            Window resultsWindow = new Window
            {
                Title = $"Scoreboard ({_game.FileName})",
                Content = resultsControl,
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            resultsWindow.ShowDialog();
        }

        private void InitializeGame()
        {
            for (int i = 0; i < 16; ++i)
            {
                Cells.Add(new CellViewModel());
            }

            IsGameStarted = false;
            UpdateState();

            try
            {
                _game.LoadResults();
            }
            catch (System.IO.FileNotFoundException)
            {
                MessageBox.Show($"{_game.FileName} does not exist and will be created",
                    "Game results file was not found", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                MessageBox.Show(
                    $"Unable to deserialize {_game.FileName} with {_game.ResultSerializer.GetType()}\nSee the debugger for the exception trace.",
                    "Game results file is invalid", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        private void InitializeTimer()
        {
            _timer = new DispatcherTimer(DispatcherPriority.Render)
            {
                Interval = TimeSpan.FromSeconds(1)
            };

            _timer.Tick += OnTimerTick;
        }

        private void OnTimerTick(object? sender, EventArgs e)
        {
            ++_seconds;
            UpdateTimeAndMoves();
        }

        private void UpdateTimeAndMoves()
        {
            var time = TimeSpan.FromSeconds(_seconds);

            CurrentTime = $"{time.Hours:D2}h : {time.Minutes:D2}m : {time.Seconds:D2}s\t{_game.GetMoves()} moves";
        }

        private void OnResetCommand(string obj)
        {
            _game.Reset();
            _seconds = 0;
            UpdateTimeAndMoves();

            _timer.Stop();
            _timer.Start();
            IsGameStarted = true;

            _mediaPlayer.Open(new Uri("Resources/Sounds/Reset.mp3", UriKind.Relative));
            _mediaPlayer.Play();

            UpdateState();
        }

        private void OnShuffleCommand(string obj)
        {
            _game.Shuffle();
            RaisePropertyChanged(nameof(Parity));

            IsResetEnabled = true;

            OnResetCommand(obj);
        }

        private void OnClickedMessage(ClickedMessage message)
        {
            var cell = message.Cell;
            var wasMoved = MakeMove(cell);

            var gameWon = IsGameStarted && _game.IsSolved();

            if (gameWon)
            {
                _timer.Stop();
                IsGameStarted = false;
                UpdateTimeAndMoves();
                PlayVictorySound();
                MessageBox.Show("Victory is yours!");
                _game.MakeRecord(_seconds);
                ViewModelUtils.TrySavingResults(_game);
            }
            else if (wasMoved)
            {
                PlayMoveSound();
            }
        }

        private void PlayVictorySound()
        {
            _mediaPlayer.Open(new Uri("Resources/Sounds/Victory.mp3", UriKind.Relative));
            _mediaPlayer.Play();
        }

        private void PlayMoveSound()
        {
            _mediaPlayer.Open(new Uri("Resources/Sounds/Move.mp3", UriKind.Relative));
            _mediaPlayer.Play();
        }

        private bool MakeMove(CellViewModel cell)
        {
            int.TryParse(cell.Title, out int cellNumber);
            var cellHexPos = Convert.ToString(cellNumber - 1, 16);
            var result = _game.Move(cellHexPos);

            UpdateState();

            return result;
        }

        private void UpdateState()
        {
            var state = _game.State;

            for (int i = 0; i < 16; ++i)
            {
                var cell = Cells[i];
                var hexName = state[i].ToString();
                var title = hexName.HexToCellName();
                cell.Title = title;
                var visibility = hexName != Game.EmptyCellHexName;
                cell.Visibility = visibility;
            }
        }
    }

    public class ViewModelUtils
    {
        public static void TrySavingResults(IGame game)
        {
            try
            {
                game.SaveResults();
            }
            catch (Exception e)
            {
                MessageBox.Show($"Unable to save results to {game.FileName}\nSee the debugger for the exception trace.",
                    "Can't write to the game results file", MessageBoxButton.OK, MessageBoxImage.Error);
                Debug.WriteLine(e);
            }
        }
    }
}