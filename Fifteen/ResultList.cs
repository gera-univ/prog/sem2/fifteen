﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fifteen
{
    [Serializable]
    public sealed class ResultList : IResultList
    {
        public List<Record> Records = new List<Record>();

        public void AddRecord(int moves, int seconds, DateTime date, string player = "unknown")
        {
            Records.Add(new Record {Moves = moves, Seconds = seconds, Date = date, Player = player});
        }

        public void DeleteRecordsBefore(DateTime date)
        {
            Records = AllRecords.Where(r => r.Date >= date).ToList();
        }

        public void MergeResultLists(IResultList other)
        {
            Records = Records.Union(other.AllRecords).ToList();
        }

        public IEnumerable<Record> AllRecords => Records;

        public IEnumerable<Record> TakeBestMovesRecords(int count = 10)
        {
            var sortedRecords = Records.OrderBy(r => r.Moves).ThenBy(r => r.Seconds).Take(count);
            return sortedRecords;
        }

        public IEnumerable<Record> TakeBestSecondsRecords(int count = 10)
        {
            var sortedRecords = Records.OrderBy(r => r.Seconds).ThenBy(r => r.Moves).Take(count);
            return sortedRecords;
        }

        public IEnumerable<Record> TakeMostRecentResults(int count = 10)
        {
            var sortedRecords = Records.OrderBy(r => r.Date).Reverse().Take(count);
            return sortedRecords;
        }

        public int CountRecordsBefore(DateTime date)
        {
            return Records.Count(r => r.Date < date);
        }
    }
}