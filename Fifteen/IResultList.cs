﻿using System;
using System.Collections.Generic;

namespace Fifteen
{
    public interface IResultList
    {
        void AddRecord(int moves, int seconds, DateTime date, string player = "unknown");
        void DeleteRecordsBefore(DateTime date);
        IEnumerable<Record> AllRecords { get; }
        IEnumerable<Record> TakeBestMovesRecords(int count = 10);
        IEnumerable<Record> TakeBestSecondsRecords(int count = 10);
        public void MergeResultLists(IResultList other);
        public IEnumerable<Record> TakeMostRecentResults(int count = 10);
        int CountRecordsBefore(DateTime date);
    }
}