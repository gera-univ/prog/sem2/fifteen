﻿using System.Globalization;

namespace Fifteen
{
    public static class GameUtils
    {
        public static string SwapChars(this string str, int pos1, int pos2)
        {
            var chars = str.ToCharArray();
            Swap(ref chars[pos1], ref chars[pos2]);
            var result = new string(chars);

            return result;
        }

        public static void Swap(ref char obj1, ref char obj2)
        {
            var tmp = obj1;
            obj1 = obj2;
            obj2 = tmp;
        }

        public static int HexToInt(this string hexPos)
            => int.Parse(hexPos, NumberStyles.HexNumber);

        public static string HexToCellName(this string hexPos)
            => (int.Parse(hexPos, NumberStyles.HexNumber) + 1).ToString();
    }
}
