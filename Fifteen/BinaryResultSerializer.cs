﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Fifteen
{
    public class BinaryResultSerializer : IResultSerializer
    {
        private readonly IFormatter _formatter = new BinaryFormatter();

        public void Save(IResultList resultList, string fileName)
        {
            using FileStream s = File.Create(fileName);
            _formatter.Serialize(s, resultList);
        }

        public IResultList Load(string fileName)
        {
            using FileStream s = File.OpenRead(fileName);
            return (ResultList) _formatter.Deserialize(s);
        }
    }
}