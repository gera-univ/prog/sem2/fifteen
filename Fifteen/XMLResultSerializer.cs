﻿using System.IO;
using System.Xml.Serialization;

namespace Fifteen
{
    public class XMLResultSerializer : IResultSerializer
    {
        XmlSerializer _xs = new XmlSerializer(typeof(ResultList));

        public void Save(IResultList resultList, string fileName)
        {
            using Stream fileStream = File.Create(fileName);
            _xs.Serialize(fileStream, resultList);
        }

        public IResultList Load(string fileName)
        {
            using Stream fileStream = File.OpenRead(fileName);
            return (IResultList)_xs.Deserialize(fileStream);
        }
    }
}