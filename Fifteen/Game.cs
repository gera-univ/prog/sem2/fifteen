﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fifteen
{
    public class Game : IGame
    {
        public const string EmptyCellHexName = "F";

        private readonly Random _random = new Random();
        public string State { get; set; }
        public string InitialState { get; set; }

        private int _movesCounter;
        private IResultList _resultList = new ResultList();

        public IResultSerializer ResultSerializer { get; set; } = new BinaryResultSerializer();
        public string FileName { get; set; } = "results.bin";

        public Game()
        {
            InitialState = "0123456789ABCDEF";
            Reset();
        }

        public void Reset()
        {
            _movesCounter = 0;
            State = InitialState;
        }

        public IEnumerable<Record> Records => _resultList.AllRecords;

        public void Shuffle()
        {
            do
            {
                var chars = InitialState.ToCharArray();

                for (int i = 1; i < chars.Length; i++)
                {
                    GameUtils.Swap(ref chars[i], ref chars[_random.Next() % i]);
                }

                InitialState = new string(chars);
                Reset();
            } while (IsSolved());
        }


        public bool IsSolved()
        {
            return State == "0123456789ABCDEF" || State == "0123456789ABCEDF";
        }

        public int GetMoves()
        {
            return _movesCounter;
        }

        public void MakeRecord(int seconds)
        {
            var startDate = DateTime.Now.AddSeconds(-seconds);
            _resultList.AddRecord(_movesCounter, seconds, startDate, Environment.UserName);
        }

        public bool IsEven()
        {
            var emptyCellPosition = GetPosition(EmptyCellHexName);
            int sum = emptyCellPosition / 4 + 1;

            for (int i = 0; i < 16; i++)
            {
                var count = 0;

                if (i == emptyCellPosition)
                    continue;

                for (int j = i + 1; j < 16; j++)
                {
                    if (j == emptyCellPosition)
                        continue;

                    if (GameUtils.HexToInt(State[j].ToString()) < GameUtils.HexToInt(State[i].ToString()))
                        ++count;
                }

                sum += count;
            }

            //Debug.WriteLine($"Sum: {sum}");
            var result = (sum % 2) == 0;

            return result;
        }

        public bool Move(string hexPos)
        {
            var currentCellPos = GetPosition(hexPos);
            var emptyCellPos = GetPosition(EmptyCellHexName);

            if (HasEmptyNeighbor(hexPos))
            {
                State = State.SwapChars(currentCellPos, emptyCellPos);
                ++_movesCounter;
                return true;
            }

            return false;
        }

        public void SaveResults()
        {
            ResultSerializer.Save(_resultList, FileName);
        }

        public void LoadResults()
        {
            _resultList = ResultSerializer.Load(FileName);
        }

        public bool HasEmptyNeighbor(string hexPos)
        {
            int emptyPos = GetPosition(EmptyCellHexName);
            int currentPos = GetPosition(hexPos);
            var result = Neighbors[currentPos].Contains(emptyPos);

            return result;
        }

        public int GetPosition(string cell)
        {
            return State.IndexOf(cell, StringComparison.OrdinalIgnoreCase);
        }

        public void MergeResultLists(IResultList otherResultList)
        {
            _resultList.MergeResultLists(otherResultList);
        }

        public IEnumerable<Record> TakeBestMovesRecords(int count = 10) => _resultList.TakeBestMovesRecords(count);
        public IEnumerable<Record> TakeBestSecondsRecords(int count = 10) => _resultList.TakeBestSecondsRecords(count);
        public IEnumerable<Record> TakeMostRecentResults(int count = 10) => _resultList.TakeMostRecentResults(count);

        public void DeleteBefore(DateTime date)
        {
            _resultList.DeleteRecordsBefore(date);
        }

        public int CountBefore(DateTime date) => _resultList.CountRecordsBefore(date);

        private static readonly IDictionary<int, IEnumerable<int>> Neighbors =
            new Dictionary<int, IEnumerable<int>>
            {
                [0] = new[] {1, 4},
                [1] = new[] {0, 5, 2},
                [2] = new[] {1, 6, 3},
                [3] = new[] {2, 7},
                [4] = new[] {0, 5, 8},
                [5] = new[] {1, 4, 6, 9},
                [6] = new[] {2, 5, 7, 10},
                [7] = new[] {3, 6, 11},
                [8] = new[] {4, 9, 12},
                [9] = new[] {5, 8, 10, 13},
                [10] = new[] {6, 9, 11, 14},
                [11] = new[] {7, 10, 15},
                [12] = new[] {8, 13},
                [13] = new[] {12, 9, 14},
                [14] = new[] {13, 10, 15},
                [15] = new[] {11, 14},
            };
    }
}