﻿using System;

namespace Fifteen
{
    [Serializable] public struct Record
    {
        public int Moves;
        public int Seconds;
        public DateTime Date;
        public string Player;

        public Record(int moves, int seconds, DateTime date, string player = "unknown")
        {
            Moves = moves;
            Seconds = seconds;
            Date = date;
            Player = player;
        }
    }
}