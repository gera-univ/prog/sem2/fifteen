﻿namespace Fifteen
{
    public interface IResultSerializer
    {
        void Save(IResultList resultList, string fileName);
        IResultList Load(string fileName);
    }
}