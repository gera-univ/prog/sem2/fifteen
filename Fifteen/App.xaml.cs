﻿using System;
using System.Windows;

namespace Fifteen
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var game = new Game();

            if (e.Args.Length >= 1)
            {
                if (e.Args.Length != 2 || e.Args[0] != "xml" && e.Args[0] != "bin")
                {
                    MessageBox.Show($"Usage:\t./{System.AppDomain.CurrentDomain.FriendlyName} xml|bin resultsFile",
                        "Command line arguments", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Current.Shutdown();
                }
                else if (e.Args[0] == "xml")
                {
                    game.ResultSerializer = new XMLResultSerializer();
                    game.FileName = e.Args[1];
                }
            }

            var mainWindow = new MainWindow();
            var viewModel = new MainWindowViewModel(game);
            mainWindow.DataContext = viewModel;
            mainWindow.Show();
        }
    }
}