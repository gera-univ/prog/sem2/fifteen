﻿using System;
using System.Collections.Generic;

namespace Fifteen
{
    public interface IGame
    {
        public string FileName { get; set; }
        public IResultSerializer ResultSerializer { get; set; }

        /// <param name="hexPos">Позиция клетки в виде шестнадцатиричного символа</param>
        bool Move(string hexPos);

        string State { get; set; }
        IEnumerable<Record> Records { get; }
        public void MergeResultLists(IResultList otherResultList);
        public IEnumerable<Record> TakeBestMovesRecords(int count = 10);
        public IEnumerable<Record> TakeBestSecondsRecords(int count = 10);
        public IEnumerable<Record> TakeMostRecentResults(int count = 10);
        void Shuffle();
        void Reset();
        bool IsEven();
        bool IsSolved();
        int GetMoves();
        public void MakeRecord(int seconds);
        public void DeleteBefore(DateTime date);
        public int CountBefore(DateTime date);
        void SaveResults();
        void LoadResults();
    }
}