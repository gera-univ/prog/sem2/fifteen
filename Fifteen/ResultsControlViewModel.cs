﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

namespace Fifteen
{
    public class ResultListItem
    {
        private string _time;
        public string Moves { get; set; }

        public string Time
        {
            get
            {
                bool result = int.TryParse(_time, out int seconds);
                return result ? new TimeSpan(0,0,seconds).ToString() : "unknown";
            }
            set => _time = value;
        }

        public string Date { get; set; }
        public string Player { get; set; }

        public ResultListItem(string moves, string time, string date, string player)
        {
            Moves = moves;
            Time = time;
            Date = date;
            Player = player;
        }
    }

    public class ResultListItemHandler
    {
        public ResultListItemHandler()
        {
            Items = new List<ResultListItem>();
        }

        public List<ResultListItem> Items { get; private set; }

        public void Add(ResultListItem resultListItem)
        {
            Items.Add(resultListItem);
        }
    }

    public class ResultsControlViewModel : ViewModelBase
    {
        private ResultListItemHandler _movesListItemHandler;
        private ResultListItemHandler _secondsListItemHandler;
        private ResultListItemHandler _recentListItemHandler;

        public List<ResultListItem> TopMovesListItems => _movesListItemHandler.Items;
        public List<ResultListItem> TopSecondsListItems => _secondsListItemHandler.Items;
        public List<ResultListItem> RecentListItems => _recentListItemHandler.Items;

        private readonly IGame _game;
        private DateTime _selectedDate = DateTime.Now.Date;

        public ICommand DeleteRecordsCommand { get; }
        public ICommand SelectSourceFileCommand { get; }

        public bool IsDeleteButtonEnabled => _game.CountBefore(SelectedDate) > 0;

        public DateTime SelectedDate
        {
            get => _selectedDate;
            set
            {
                _selectedDate = value;
                RaisePropertyChanged(nameof(IsDeleteButtonEnabled));
            }
        }

        public List<string> Formats { get; set; } = new List<string>
        {
            "BIN", "XML"
        };

        public string SelectedFormat { get; set; } = "BIN";

        public ResultsControlViewModel(IGame game)
        {
            _game = game;

            DeleteRecordsCommand = new RelayCommand<string>(OnDeleteRecords);
            SelectSourceFileCommand = new RelayCommand<string>(OnSelectCommand);

            UpdateListItems();
        }

        private void OnSelectCommand(string obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
                {Filter = $"{SelectedFormat} files (*.{SelectedFormat})|*.{SelectedFormat}|All files (*.*)|*.*"};
            if (openFileDialog.ShowDialog() == true)
            {
                IResultSerializer serializer = new BinaryResultSerializer();
                if (SelectedFormat == "XML")
                {
                    serializer = new XMLResultSerializer();
                }

                try
                {
                    var sourceList = serializer.Load(openFileDialog.FileName);
                    _game.MergeResultLists(sourceList);
                    UpdateListItems();
                    _game.SaveResults();
                }
                catch (Exception)
                {
                    MessageBox.Show($"Failed to deserialize {openFileDialog.FileName} with {serializer.GetType()}",
                        "Merge error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void UpdateListItems()
        {
            _movesListItemHandler = new ResultListItemHandler();
            _secondsListItemHandler = new ResultListItemHandler();
            _recentListItemHandler = new ResultListItemHandler();

            foreach (var record in _game.TakeBestMovesRecords())
            {
                _movesListItemHandler.Add(new ResultListItem(record.Moves.ToString(), record.Seconds.ToString(),
                    record.Date.ToString(), record.Player));
            }

            foreach (var record in _game.TakeBestSecondsRecords())
            {
                _secondsListItemHandler.Add(new ResultListItem(record.Moves.ToString(), record.Seconds.ToString(),
                    record.Date.ToString(), record.Player));
            }

            foreach (var record in _game.TakeMostRecentResults())
            {
                _recentListItemHandler.Add(new ResultListItem(record.Moves.ToString(), record.Seconds.ToString(),
                    record.Date.ToString(), record.Player));
            }

            RaisePropertyChanged(nameof(TopMovesListItems));
            RaisePropertyChanged(nameof(TopSecondsListItems));
            RaisePropertyChanged(nameof(RecentListItems));
            RaisePropertyChanged(nameof(IsDeleteButtonEnabled));
        }

        private void OnDeleteRecords(string obj)
        {
            _game.DeleteBefore(SelectedDate);
            UpdateListItems();
            ViewModelUtils.TrySavingResults(_game);

            RaisePropertyChanged(nameof(IsDeleteButtonEnabled));
        }
    }
}