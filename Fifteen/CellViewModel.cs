﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;

namespace Fifteen
{
    public class CellViewModel : ViewModelBase
    {
        private bool _visibility;
        private string _title;

        public bool Visibility
        {
            get => _visibility;
            set
            {
                if (_visibility != value)
                {
                    _visibility = value;
                    RaisePropertyChanged(nameof(Visibility));
                }
            }
        }

        public string Title
        {
            get => _title;
            set
            {
                if (_title != value)
                {
                    _title = value;
                    RaisePropertyChanged(nameof(Title));
                }
            }
        }

        public CellViewModel()
        {
            ClickCommand = new RelayCommand<string>(OnClickCommand);
        }

        public ICommand ClickCommand { get; }

        private void OnClickCommand(string param)
        {
            var message = new ClickedMessage { Cell = this };
            MessengerInstance.Send(message);
        }
    }
}
